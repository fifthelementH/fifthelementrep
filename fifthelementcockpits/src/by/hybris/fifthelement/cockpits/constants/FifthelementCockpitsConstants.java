/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *  
 */
package by.hybris.fifthelement.cockpits.constants;

/**
 * Global class for all FifthelementCockpits constants. You can add global constants for your extension into this class.
 */
public final class FifthelementCockpitsConstants extends GeneratedFifthelementCockpitsConstants
{
	public static final String EXTENSIONNAME = "fifthelementcockpits";

	public final static String JASPER_REPORTS_MEDIA_FOLDER = "jasperreports";

	private FifthelementCockpitsConstants()
	{
		// empty
	}
}
