/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package by.hybris.fifthelement.initialdata.setup;

import de.hybris.platform.commerceservices.dataimport.impl.CoreDataImportService;
import de.hybris.platform.commerceservices.dataimport.impl.SampleDataImportService;
import de.hybris.platform.commerceservices.setup.AbstractSystemSetup;
import de.hybris.platform.commerceservices.setup.data.ImportData;
import de.hybris.platform.commerceservices.setup.events.CoreDataImportedEvent;
import de.hybris.platform.commerceservices.setup.events.SampleDataImportedEvent;
import de.hybris.platform.core.initialization.SystemSetup;
import de.hybris.platform.core.initialization.SystemSetup.Process;
import de.hybris.platform.core.initialization.SystemSetup.Type;
import de.hybris.platform.core.initialization.SystemSetupContext;
import de.hybris.platform.core.initialization.SystemSetupParameter;
import de.hybris.platform.core.initialization.SystemSetupParameterMethod;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import by.hybris.fifthelement.initialdata.constants.FifthelementInitialDataConstants;


/**
 * This class provides hooks into the system's initialization and update processes.
 *
 * @see "https://wiki.hybris.com/display/release4/Hooks+for+Initialization+and+Update+Process"
 */
@SystemSetup(extension = FifthelementInitialDataConstants.EXTENSIONNAME)
public class InitialDataSystemSetup extends AbstractSystemSetup
{
	@SuppressWarnings("unused")
	private static final Logger LOG = Logger.getLogger(InitialDataSystemSetup.class);

	private static final String IMPORT_CORE_DATA = "importCoreData";
	private static final String IMPORT_SAMPLE_DATA = "importSampleData";
	private static final String ACTIVATE_SOLR_CRON_JOBS = "activateSolrCronJobs";

	private CoreDataImportService coreDataImportService;
	private SampleDataImportService sampleDataImportService;


	public static final String CREATE_STORE_PARAM = "createStore";
	public static final String ALL_STORES = "all";
	public static final String STORE_HYBRIS = "hybris";
	public static final String STORE_FIFTH_ELEMENT = "fe";

	/**
	 * Generates the Dropdown and Multi-select boxes for the project data import
	 */
	@Override
	@SystemSetupParameterMethod
	public List<SystemSetupParameter> getInitializationOptions()
	{
		final List<SystemSetupParameter> params = new ArrayList<SystemSetupParameter>();

		final SystemSetupParameter imports = new SystemSetupParameter(CREATE_STORE_PARAM);
		imports.setLabel(CREATE_STORE_PARAM);
		imports.addValues(new String[]
		{ ALL_STORES, STORE_FIFTH_ELEMENT, STORE_HYBRIS });
		params.add(imports);

		params.add(createBooleanSystemSetupParameter(IMPORT_CORE_DATA, "Import Core Data", true));
		params.add(createBooleanSystemSetupParameter(IMPORT_SAMPLE_DATA, "Import Sample Data", true));
		params.add(createBooleanSystemSetupParameter(ACTIVATE_SOLR_CRON_JOBS, "Activate Solr Cron Jobs", true));

		return params;
	}

	/**
	 * Implement this method to create initial objects. This method will be called by system creator during
	 * initialization and system update. Be sure that this method can be called repeatedly.
	 *
	 * @param context
	 *           the context provides the selected parameters and values
	 */
	@SystemSetup(type = Type.ESSENTIAL, process = Process.ALL)
	public void createEssentialData(final SystemSetupContext context)
	{
		// Add Essential Data here as you require
	}

	private List<String> getParams(final SystemSetupContext context, final String param)
	{
		String[] params = context.getParameters(param);
		if (params == null)
		{
			params = context.getParameters(FifthelementInitialDataConstants.EXTENSIONNAME + "_" + param);
		}
		if (params == null)
		{
			return Collections.EMPTY_LIST;
		}
		return Arrays.asList(params);
	}

	/**
	 * Implement this method to create data that is used in your project. This method will be called during the system
	 * initialization.
	 *
	 * @param context
	 *           the context provides the selected parameters and values
	 */
	@SystemSetup(type = Type.PROJECT, process = Process.ALL)
	public void createProjectData(final SystemSetupContext context)
	{
		final List<ImportData> importData = new ArrayList<ImportData>();

		final List<String> stores = new ArrayList<String>();
		final List<String> params = getParams(context, CREATE_STORE_PARAM);

		if (Process.INIT.equals(context.getProcess()))
		{
			stores.add(STORE_HYBRIS);
			stores.add(STORE_FIFTH_ELEMENT);
		}
		else
		{
			if (params.contains(STORE_HYBRIS))
			{
				stores.add(STORE_HYBRIS);
			}
			else if (params.contains(STORE_FIFTH_ELEMENT))
			{
				stores.add(STORE_FIFTH_ELEMENT);
			}
			else if (params.contains(ALL_STORES))
			{
				stores.add(STORE_HYBRIS);
				stores.add(STORE_FIFTH_ELEMENT);
			}
		}

		for (final String store : stores)
		{
			final ImportData sampleImportData = new ImportData();
			sampleImportData.setProductCatalogName(store);
			sampleImportData.setContentCatalogNames(Arrays.asList(store));
			sampleImportData.setStoreNames(Arrays.asList(store));
			importData.add(sampleImportData);
		}

		getCoreDataImportService().execute(this, context, importData);
		getEventService().publishEvent(new CoreDataImportedEvent(context, importData));

		getSampleDataImportService().execute(this, context, importData);
		getEventService().publishEvent(new SampleDataImportedEvent(context, importData));

	}

	public CoreDataImportService getCoreDataImportService()
	{
		return coreDataImportService;
	}

	@Required
	public void setCoreDataImportService(final CoreDataImportService coreDataImportService)
	{
		this.coreDataImportService = coreDataImportService;
	}

	public SampleDataImportService getSampleDataImportService()
	{
		return sampleDataImportService;
	}

	@Required
	public void setSampleDataImportService(final SampleDataImportService sampleDataImportService)
	{
		this.sampleDataImportService = sampleDataImportService;
	}
}
