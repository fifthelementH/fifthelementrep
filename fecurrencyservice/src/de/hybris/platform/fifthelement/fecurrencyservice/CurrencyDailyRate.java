/**
 *
 */
package de.hybris.platform.fifthelement.fecurrencyservice;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.squareup.okhttp.Headers;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;


/**
 * @author TagirovA
 *
 */

public class CurrencyDailyRate
{
	public static void main(final String[] args)
	{
		final SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		final String today = format.format(new Date());

		final OkHttpClient client = new OkHttpClient();
		final Request request = constructRequest("http://www.nbrb.by/Services/XmlExRates.aspx?ondate=" + today, null);
		try
		{
			final Response response = client.newCall(request).execute();
			final InputStream is = response.body().byteStream();

			// Document doc = readXml(is);

			// System.out.println(is.read());


			final BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
			String temp = br.readLine();
			final StringBuilder sb = new StringBuilder();
			while (temp != null)
			{
				sb.append(temp);
				temp = br.readLine();
			}

			System.out.println("���� �� �������:" + sb);

			// } catch (IOException | SAXException | ParserConfigurationException e) {
		}
		catch (final IOException e)
		{
			e.printStackTrace();
		}


		//CurrencyModel m;//= //....
		////NBRB
		//NBRB
		//	m.setConversion(kurs);

	}

	/**
	 * Read XML as DOM.
	 */
	public static Document readXml(final InputStream is) throws SAXException, IOException, ParserConfigurationException
	{
		final DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

		dbf.setValidating(false);
		dbf.setIgnoringComments(false);
		dbf.setIgnoringElementContentWhitespace(true);
		dbf.setNamespaceAware(true);
		// dbf.setCoalescing(true);
		// dbf.setExpandEntityReferences(true);

		DocumentBuilder db = null;
		db = dbf.newDocumentBuilder();
		//db.setEntityResolver(new NullResolver());


		// db.setErrorHandler( new MyErrorHandler());

		return db.parse(is);
	}



	public static Request constructRequest(final String url, final Headers headers)
	{
		final Request.Builder builder = new Request.Builder().url(url).get();

		return builder.build();
	}
}
