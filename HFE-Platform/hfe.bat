setlocal enableextensions

set UPDATE_PATH_BASE=\\issoft\hybris\HFE
set LOCAL_HFE_DIR=%HFE_HOME%
set LOCAL_HFE_ECLIPSE_DIR=%HFE_HOME%\eclipse

set JAVA_EXEC=%LOCAL_HFE_DIR%\jdk7\jre\bin\server\jvm.dll

set CONFIGURE_SCRIPT=configure_hfe.bat
set INSTALLER=eclipse-inst-win64.exe
set LOCAL_HFE_ZIP=%LOCAL_HFE_DIR%\HFE.zip
set LOCAL_UNZIPPER=%LOCAL_HFE_DIR%\unzip.vbs
set HFE_PRODUCT_FILE=user.products.setup
set LOCAL_HFE_PRODUCT_DIR=%SystemDrive%\Users\%USERNAME%\.eclipse\org.eclipse.oomph.setup\setups
set LOCAL_HFE_PRODUCT_FILE=%LOCAL_HFE_PRODUCT_DIR%\%HFE_PRODUCT_FILE%


rem INSTALL AND CONFIGURE HFE IF NECESSARY
if exist "%LOCAL_HFE_ECLIPSE_DIR%\eclipse.exe" goto :eclipse_start
if exist "%LOCAL_HFE_ECLIPSE_DIR%" rmdir /s /q "%LOCAL_HFE_ECLIPSE_DIR%" || goto :error
echo You do not have HFE installed.
echo It will get installed now, please wait and do not close this window.
echo This can take a while...
xcopy "%UPDATE_PATH_BASE%\HFE.zip" "%LOCAL_HFE_DIR%" /I /R /Y /D /Z /Q || goto :error
xcopy "%UPDATE_PATH_BASE%\unzip.vbs" "%LOCAL_HFE_DIR%" /I /R /Y /D /Z /Q || goto :error
xcopy "%UPDATE_PATH_BASE%\%CONFIGURE_SCRIPT%" "%LOCAL_HFE_DIR%" /I /R /Y /D /Z /Q || goto :error
if not exist %LOCAL_HFE_PRODUCT_DIR% mkdir %LOCAL_HFE_PRODUCT_DIR% 
xcopy "%UPDATE_PATH_BASE%\%HFE_PRODUCT_FILE%" "%LOCAL_HFE_PRODUCT_DIR%" /I /R /Y /D /Z /Q || goto :error
echo HFE zip and configuration files have been downloaded, now unzipping...
cscript //nologo %LOCAL_UNZIPPER% "%LOCAL_HFE_ZIP%" "%LOCAL_HFE_DIR%" || goto :error
if exist "%LOCAL_UNZIPPER%" del /f /q "%LOCAL_UNZIPPER%"
if exist "%LOCAL_HFE_ZIP%" del /f /q "%LOCAL_HFE_ZIP%"
echo Unzipping done. Now configuring...
Call %LOCAL_HFE_DIR%\%CONFIGURE_SCRIPT% || goto :error
echo Configuration done. Now starting eclipse installer...
start %LOCAL_HFE_DIR%\%INSTALLER% || goto :error
goto :exit_success


:eclipse_start
echo Copying HFE updates...
xcopy "%UPDATE_PATH_BASE%\eclipse" "%LOCAL_HFE_ECLIPSE_DIR%" /I /R /Y /D /Z || goto :error
for /f "tokens=*" %%a in (
	'xcopy "%UPDATE_PATH_BASE%\%CONFIGURE_SCRIPT%" "%LOCAL_HFE_DIR%" /I /R /Y /D /Z'
) do (
	set COPY_RESULT=%%a
)
echo %COPY_RESULT%
if not "%COPY_RESULT%" == "0 Datei(en) kopiert" Call %LOCAL_HFE_DIR%\%CONFIGURE_SCRIPT%
echo Starting HFE eclipse...
start "HFE" /max "%LOCAL_HFE_ECLIPSE_DIR%\eclipse.exe" %1 %2 %3 %4 %5 -vm %JAVA_EXEC% -data "%~dp0\workspace"
goto :exit_success


:exit_success
echo Done.
echo Press ENTER to close this window.
pause > nul
exit /B 0

:error
echo Unexpected error during installing and configuring HFE.
echo Press ENTER to close this window.
pause > nul
exit /B 1

endlocal 
