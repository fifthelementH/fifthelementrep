rem DEFINE ENVIRONMENT VARIABLES
echo Configuring environment variables...
set HFE_HOME=%cd%
set JAVA_HOME=%HFE_HOME%\jdk7
set HYBRIS_HOME=%HFE_HOME%\hybris\hybris\bin
set ANT_OPTS=-Xmx200m -XX:MaxPermSize=128M
set ANT_HOME=%HYBRIS_HOME%\platform\apache-ant-1.9.1
set MAVEN_HOME=%HFE_HOME%\maven
set PATH=%PATH%;%JAVA_HOME%\bin;%ANT_HOME%\bin;%MAVEN_HOME%\bin

rem EXPORT ENVIRONMENT VARIABLES
setx HFE_HOME %cd%
setx JAVA_HOME %HFE_HOME%\jdk7
setx HYBRIS_HOME %HFE_HOME%\hybris\hybris\bin
setx ANT_HOME %HYBRIS_HOME%\platform\apache-ant-1.9.1
setx MAVEN_HOME=%HFE_HOME%\maven

rem EXTEND PATH VARIABLES WITH DEFINED ENVIRONMENT VARIABLES
echo Updating PATH variable...
setx PATH %JAVA_HOME%\bin;%ANT_HOME%\bin;%MAVEN_HOME%\bin

