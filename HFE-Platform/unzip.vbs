if Wscript.Arguments.Count <> 2 then
    WScript.Echo "Missing parameters"
	Wscript.QUIT  1
end if

set fso = CreateObject("Scripting.FileSystemObject")
ZipFile = fso.GetAbsolutePathName(WScript.Arguments.Item(0))
Dest = fso.GetAbsolutePathName(WScript.Arguments.Item(1))

set objShell = CreateObject("Shell.Application")
set FilesInZip = objShell.NameSpace(ZipFile).items
objShell.NameSpace(Dest).CopyHere(FilesInZip)

set fso = Nothing
set objShell = Nothing

Wscript.QUIT