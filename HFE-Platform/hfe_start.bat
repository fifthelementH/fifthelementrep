@echo off

rem /**
rem  * Generic batch file used to start FIDE.
rem  * Batch checks for an updated version of fide.bat before launching HFE
rem  */
set UPDATE_BASE_PATH=\\issoft\hybris\HFE
set UPDATE_HFE=%UPDATE_BASE_PATH%\hfe.bat
set HFE_DIR_BASE=%HFE_HOME%
set LOCAL_HFE=%HFE_DIR_BASE%\hfe.bat

rem /**
rem  * Check if an updated version of hfe.bat exists on the issoft share
rem  */
xcopy "%UPDATE_HFE%" "%HFE_DIR_BASE%" /I /R /Y /D /Z /Q

rem /**
rem  * Start HFE ECLIPSE
rem  */
call "%LOCAL_HFE%" %1 %2 %3 %4 %5
