/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *  
 */
package by.hybris.fifthelement.facades.constants;

/**
 * Global class for all FifthelementFacades constants.
 */
@SuppressWarnings("PMD")
public class FifthelementFacadesConstants extends GeneratedFifthelementFacadesConstants
{
	public static final String EXTENSIONNAME = "fifthelementfacades";

	private FifthelementFacadesConstants()
	{
		//empty
	}
}
