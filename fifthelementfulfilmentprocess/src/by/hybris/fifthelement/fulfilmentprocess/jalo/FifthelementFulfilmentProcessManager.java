/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *  
 */
package by.hybris.fifthelement.fulfilmentprocess.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import by.hybris.fifthelement.fulfilmentprocess.constants.FifthelementFulfilmentProcessConstants;

import org.apache.log4j.Logger;

@SuppressWarnings("PMD")
public class FifthelementFulfilmentProcessManager extends GeneratedFifthelementFulfilmentProcessManager
{
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger( FifthelementFulfilmentProcessManager.class.getName() );
	
	public static final FifthelementFulfilmentProcessManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (FifthelementFulfilmentProcessManager) em.getExtension(FifthelementFulfilmentProcessConstants.EXTENSIONNAME);
	}
	
}
